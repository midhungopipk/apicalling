class Character {
	constructor(
		_id,
		url,
		name,
		films,
		shortFilms,
		tvShows,
		videoGames,
		imageUrl,
		parkAttractions,
		allies,
		enemies,
		date
	) {
		this.id = _id;
		this.url = url;
		this.name = name;
		this.films = films;
		this.shortFilms = shortFilms;
		this.tvShows = tvShows;
		this.videoGames = videoGames;
		this.imageUrl = imageUrl;
		this.parkAttractions = parkAttractions;
		this.allies = allies;
		this.enemies = enemies;
		this.date = date;
	}
}

export default Character;
