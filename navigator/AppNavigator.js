import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";

import DetailedViewScreen from "../screens/DetailedViewScreen";
import ListViewScreen from "../screens/ListViewScreen";
import HistoryScreen from "../screens/HistoryScreen";

const defaultNavoptions = {
	headerStyle: {
		backgroundColor: Platform.OS === "android" ? "skyblue" : "",
	},
	headerTintColor: Platform.OS === "android" ? "white" : "grey",
};

const DataNavigator = createStackNavigator(
	{
		Characters: ListViewScreen,
		CharactersDetails: DetailedViewScreen,
	},
	{
		defaultNavigationOptions: defaultNavoptions,
	}
);

const HistoryNavigator = createStackNavigator(
	{
		History: HistoryScreen,
	},
	{
		defaultNavigationOptions: defaultNavoptions,
	}
);

const DrawerNavigator = createDrawerNavigator(
	{
		Characters: DataNavigator,
		History: HistoryNavigator,
	},
	{
		contentOptions: {
			activeTintColor: "navy",
			labelStyle: {
				marginVertical: 40,
				fontSize: 15,
			},
		},
	}
);

export default createAppContainer(DrawerNavigator);
