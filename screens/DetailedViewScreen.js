import AsyncStorage from "@react-native-async-storage/async-storage";
import { LinearGradient } from "expo-linear-gradient";
import React, { useEffect } from "react";
import { View, Text, StyleSheet, Image, ScrollView } from "react-native";
import { useSelector } from "react-redux";

const ListItem = (props) => {
	return (
		<View style={styles.listItem}>
			<Text>{props.children}</Text>
		</View>
	);
};

const DetailedViewScreen = (props) => {
	const selectedCharId = props.navigation.getParam("charId");

	const selectedCharacter = useSelector((state) =>
		state.characters.allCharacters.find(
			(char) => char.id === selectedCharId
		)
	);

	const fetch = async () => {
		try {
			const History = await AsyncStorage.getItem("historyData");
			const transformedHistory = JSON.parse(History);

			if (transformedHistory === null) {
				let historyArray = [];
				selectedCharacter.date = new Date().toLocaleString();
				historyArray.push(selectedCharacter);
				await AsyncStorage.setItem(
					"historyData",
					JSON.stringify(historyArray)
				);
			} else {
				selectedCharacter.date = new Date().toLocaleString();
				const historyArray = [...transformedHistory, selectedCharacter];
				await AsyncStorage.setItem(
					"historyData",
					JSON.stringify(historyArray)
				);
			}
		} catch (error) {
			console.log(error);
		}
	};
	useEffect(() => {
		fetch();
	}, []);

	return (
		<LinearGradient colors={["skyblue", "white"]}>
			<ScrollView style={{ paddingBottom: 30 }}>
				<Image
					style={styles.image}
					source={{ uri: selectedCharacter.imageUrl }}
				/>
				<Text style={styles.name}>{selectedCharacter.name}</Text>

				<Text style={styles.title}>Films</Text>
				{selectedCharacter.films.length !== 0 ? (
					<View style={styles.title}>
						{selectedCharacter.films.map((films) => (
							<ListItem key={films}>{films}</ListItem>
						))}
					</View>
				) : (
					<Text style={{ color: "red", textAlign: "center" }}>
						Sorry!! No Film to Show
					</Text>
				)}

				<Text style={styles.title}>Short Films</Text>
				{selectedCharacter.shortFilms.length !== 0 ? (
					<View style={styles.title}>
						{selectedCharacter.shortFilms.map((shortFilms) => (
							<ListItem key={shortFilms}>{shortFilms}</ListItem>
						))}
					</View>
				) : (
					<Text style={{ color: "red", textAlign: "center" }}>
						Sorry!! No ShortFilm to Show
					</Text>
				)}

				<Text style={styles.title}>Tv Shows</Text>
				{selectedCharacter.tvShows.length !== 0 ? (
					<View style={styles.title}>
						{selectedCharacter.tvShows.map((tvShows) => (
							<ListItem key={tvShows}>{tvShows}</ListItem>
						))}
					</View>
				) : (
					<Text style={{ color: "red", textAlign: "center" }}>
						Sorry!! No Tv Shows to Show
					</Text>
				)}

				<Text style={styles.title}>Video Games</Text>
				{selectedCharacter.videoGames.length !== 0 ? (
					<View style={styles.title}>
						{selectedCharacter.videoGames.map((videoGames) => (
							<ListItem key={videoGames}>{videoGames}</ListItem>
						))}
					</View>
				) : (
					<Text style={{ color: "red", textAlign: "center" }}>
						sorry!! No Video Games to Show
					</Text>
				)}

				<Text style={styles.title}>
					Allies of {selectedCharacter.name}
				</Text>
				{selectedCharacter.allies.length !== 0 ? (
					<View style={styles.title}>
						{selectedCharacter.allies.map((allies) => (
							<ListItem key={allies}>{allies}</ListItem>
						))}
					</View>
				) : (
					<Text style={{ color: "red", textAlign: "center" }}>
						Sorry!! No Allies to Show
					</Text>
				)}

				<Text style={styles.title}>
					Enemies of {selectedCharacter.name}
				</Text>
				{selectedCharacter.enemies.length !== 0 ? (
					<View style={styles.title}>
						{selectedCharacter.enemies.map((enemies) => (
							<ListItem key={enemies}>{enemies}</ListItem>
						))}
					</View>
				) : (
					<Text style={{ color: "red", textAlign: "center" }}>
						Sorry!! No Enemies to Show
					</Text>
				)}
			</ScrollView>
		</LinearGradient>
	);
};
DetailedViewScreen.navigationOptions = (navData) => {
	return {
		headerTitle: navData.navigation.getParam("charName"),
	};
};
const styles = StyleSheet.create({
	image: {
		width: "100%",
		minHeight: 400,
		resizeMode: "contain",
		marginTop: 9,
		borderRadius: 5,
	},
	name: {
		fontSize: 28,
		fontWeight: "bold",
		textAlign: "center",
		marginVertical: 20,
	},
	title: {
		fontSize: 25,
		fontWeight: "bold",
		color: "navy",
		textAlign: "center",
		marginTop: 20,
	},
	titleDescriptions: {
		fontSize: 22,
		fontWeight: "bold",
		textAlign: "center",
		marginVertical: 20,
	},
	listItem: {
		marginVertical: 10,
		marginHorizontal: 20,
		borderColor: "grey",
		borderWidth: 2,
		padding: 10,
	},
});

export default DetailedViewScreen;
