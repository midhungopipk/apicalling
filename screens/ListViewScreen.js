import React, { useEffect } from "react";
import {
	View,
	Text,
	FlatList,
	StyleSheet,
	Platform,
	TouchableNativeFeedback,
	Image
} from "react-native";
import HeaderButton from "../UI/HeaderButtons";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import * as characterActions from "../store/action/character";
import { useDispatch, useSelector } from "react-redux";
import { LinearGradient } from "expo-linear-gradient";

const ListViewScreen = (props) => {
	const characters = useSelector((state) => state.characters.allCharacters);
	const dispatch = useDispatch();

	useEffect(async () => {
		await dispatch(characterActions.fetchData());
	}, [dispatch]);

	const renderData = (itemData) => {
		return (
			<View style={styles.characterContainer}>
				<View style={styles.touchable}>
					<TouchableNativeFeedback
						onPress={() => {
							props.navigation.navigate({
								routeName: "CharactersDetails",
								params: {
									charId: itemData.item.id,
									charName: itemData.item.name,
								},
							});
						}}
					>
						<LinearGradient colors={["skyblue", "white"]}>
							<View>
								<View style={styles.imageContainer}>
									<Image
										style={styles.image}
										source={{ uri: itemData.item.imageUrl }}
									/>
								</View>
								<View style={styles.nameContainer}>
									<Text style={styles.name}>
										{itemData.item.name}
									</Text>
								</View>
							</View>
						</LinearGradient>
					</TouchableNativeFeedback>
				</View>
			</View>
		);
	};
	
	return (
		<LinearGradient colors={["skyblue", "white"]}>
			<FlatList
				data={characters}
				keyExtractor={(item, index) => item.id}
				renderItem={renderData}
			/>
		</LinearGradient>
	);
};

ListViewScreen.navigationOptions = (navData) => {
	return {
		headerTitle: "All Characters",
		headerLeft: () => {
			return (
				<HeaderButtons HeaderButtonComponent={HeaderButton}>
					<Item
						title="Menu"
						iconName={
							Platform.OS === "android" ? "md-menu" : "ios-menu"
						}
						onPress={() => {
							navData.navigation.toggleDrawer();
						}}
					/>
				</HeaderButtons>
			);
		},
	};
};

const styles = StyleSheet.create({
	characterContainer: {
		elevation: 6,
		margin: 20,
		height: 300,
		width: "90%",
		borderRadius: 25,
		backgroundColor: "white",
	},
	touchable: { borderRadius: 25, overflow: "hidden" },
	imageContainer: {
		width: "100%",
		height: "75%",
		borderTopLeftRadius: 25,
		borderTopRightRadius: 25,
		overflow: "hidden",
	},
	image: {
		width: "100%",
		minHeight: "100%",
		flex: 1,
		resizeMode: "contain",
	},
	name: {
		fontSize: 18,
		marginVertical: 4,
		fontWeight: "bold",
	},
	nameContainer: {
		alignItems: "center",
		height: 100,
	},
	loader:{
		marginVertical:10,
		alignItems:'center'
	}
});

export default ListViewScreen;
