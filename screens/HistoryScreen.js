import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useEffect, useState } from "react";
import {
	View,
	Text,
	StyleSheet,
	Platform,
	FlatList,
	Image,
} from "react-native";
import HeaderButton from "../UI/HeaderButtons";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

const HistoryScreen = (props) => {
	const [historyData, setHistoryData] = useState();
	
	const fetchHistoryData=async()=>{
		const history = await AsyncStorage.getItem("historyData");
		const updatedHistory = JSON.parse(history);
		setHistoryData(updatedHistory);
	}
	useEffect(()=>{
		const willFocusSub = props.navigation.addListener("willFocus",fetchHistoryData);
		return () => {
			willFocusSub.remove();
		};
	},[fetchHistoryData])

	const historyViewHandler = (itemData) => {
		return (
			<View style={styles.characterContainer}>
				<View style={styles.touchable}>
					<Text style={styles.name}>{itemData.item.name}</Text>
					<View style={styles.details}>
						<Image
							style={styles.image}
							source={{ uri: itemData.item.imageUrl }}
						/>
						<Text>Viewed on {itemData.item.date}</Text>
					</View>
				</View>
			</View>
		);
	};

	return (
		<FlatList
			inverted
			data={historyData}
			keyExtractor={(item, index) => item.date}
			renderItem={historyViewHandler}
		/>
	);
};
HistoryScreen.navigationOptions = (navData) => {
	return {
		headerTitle: "History",
		headerLeft: () => {
			return (
				<HeaderButtons HeaderButtonComponent={HeaderButton}>
					<Item
						title="Menu"
						iconName={
							Platform.OS === "android" ? "md-menu" : "ios-menu"
						}
						onPress={() => {
							navData.navigation.toggleDrawer();
						}}
					/>
				</HeaderButtons>
			);
		},
	};
};
const styles = StyleSheet.create({
	characterContainer: {
		elevation: 6,
		margin: 20,
		height: 200,
		width: "90%",
		borderRadius: 25,
		backgroundColor: "white",
	},
	touchable: {
		borderRadius: 25,
		overflow: "hidden",
	},
	name: {
		fontSize: 22,
		marginVertical: 4,
		fontWeight: "bold",
		textAlign: "center",
	},
	image: {
		height: 120,
		width: 150,
		resizeMode: "contain",
	},
	details: {
		alignItems: "center",
		justifyContent: "center",
		marginVertical: 10,
	},
});

export default HistoryScreen;
