import Character from "../../Model/character";

export const FETCH_DATA = "FETCH_DATA";

export const fetchData = () => {
	return async (dispatch) => {
		const response = await fetch("https://api.disneyapi.dev/characters");
		const resData = await response.json();
		const date = new Date();
		const allCharacters = [];
		for (const key in resData.data) {
			allCharacters.push(
				new Character(
					resData.data[key]._id,
					resData.data[key].url,
					resData.data[key].name,
					resData.data[key].films,
					resData.data[key].shortFilms,
					resData.data[key].tvShows,
					resData.data[key].videoGames,
					resData.data[key].imageUrl,
					resData.data[key].parkAttractions,
					resData.data[key].allies,
					resData.data[key].enemies,
					date.toISOString()
				)
			);
		}

		dispatch({ type: FETCH_DATA, characters: allCharacters });
	};
};
