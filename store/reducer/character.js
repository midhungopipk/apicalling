import { FETCH_DATA } from "../action/character";

const initialState = {
	allCharacters: [],
};

export default (state = initialState, action) => {
	switch (action.type) {
		case FETCH_DATA:
			return { ...state, allCharacters: action.characters };
	}
	return state;
};
