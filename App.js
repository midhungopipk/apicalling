import { StyleSheet, Text, View } from "react-native";
import { combineReducers, createStore, applyMiddleware } from "redux";
import AppNavigator from "./navigator/AppNavigator";
import { Provider } from "react-redux";
import ReduxThunk from "redux-thunk";

import characterReducer from "./store/reducer/character";

const rootReducer = combineReducers({
	characters: characterReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
	return (
		<Provider store={store}>
			<AppNavigator />
		</Provider>
	);
}

const styles = StyleSheet.create({});
